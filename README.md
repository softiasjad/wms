# Finestmedia

This is a repo for calculating MobileStation coordinates using detected distances from BaseStations.

## Scope

#####Required scope
For required scope and functional description look *test_task_JAVA.pdf*.

#####Additional functionality
MessageService provides additional opportunity to use .properties keywords for messages.
ResourceBundleMessageSource provides additional opportunity to add messages' translations (i18) according to locale. Not dynamic at the moment though.
Default locale is set to Estonian "et". English is also supported.
````bash
http://localhost:8080/wms?lang=en
````

## Usage

###Requirements
Java 8 version installed

###Technologies used
Project is bases on [Spring initializr](https://start.spring.io/) quick-start package:
Spring boot 2.0.5, Java, Maven, Web, REST API, Spring Date JPA, H2.

Package includes built-in Tomcat 7 plugin.

###Building and running the application
````bash
mvn clean install
mvn spring-boot:run
````
Get the list of all detections reported by all BS.
````bash
http://localhost:8080/wms
````
Detections are sorted by MobileStation ID. Look for msId that repeats at least three times in a row and use it in:
````bash
http://localhost:8080/location/{msId}
````
