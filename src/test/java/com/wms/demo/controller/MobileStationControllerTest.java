package com.wms.demo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wms.demo.dto.MobileStationDto;
import com.wms.demo.entity.BaseStation;
import com.wms.demo.entity.DetectedMs;
import com.wms.demo.service.BaseStationService;
import com.wms.demo.service.DetectedMSService;
import com.wms.demo.service.SetUp;
import com.wms.demo.utils.Constants;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.wms.demo.utils.Constants.DETECTION_RADIUS_IN_METERS;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class MobileStationControllerTest {

    @Autowired
    SetUp setUp;
    @Autowired
    MockMvc mvc;
    @Autowired
    MobileStationController mobileStationController;
    @Autowired
    BaseStationService baseStationService;
    @Autowired
    DetectedMSService detectedMSService;
    UUID msUuid;
    long timestamp;
    @Autowired
    private TestRestTemplate restTemplate;


    @Before
    public void setup() throws JsonProcessingException {
        this.msUuid = UUID.fromString("46614507-61fd-45a2-b3bd-bd71fbb34c55");
        this.timestamp = Instant.now().getEpochSecond();

        createBaseStations();
        List<MobileStationDto> msList = createMobileStations();
        DetectMobileStations(msList);

        /*ObjectMapper mapper = new ObjectMapper();
        System.out.println(String.format(
                "NB! getAllBaseStations: %s", mapper.writeValueAsString(baseStationService.getAllBaseStations())));
        System.out.println(
                String.format("NB! msList: %s", mapper.writeValueAsString(msList)));
        System.out.println(
                String.format("NB! Detected: %s", mapper.writeValueAsString(detectedMSService.getAllDetections())));*/
    }

    @Test
    public void getMobileStationById() throws Exception {
        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.get(String.format("/location/%s", this.msUuid)).accept(MediaType.APPLICATION_JSON);
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andReturn();

        System.out.println(
                String.format("NB! Actual response GET {msId}: %s", result.getResponse().getContentAsString()));

        String expected = "{\"mobileId\": " + this.msUuid + ", " +
                "\"x\": 5," +
                "\"y\": 4," +
                "\"error_radius\": 0," +
                "\"error_code\": 0," +
                "\"error_description\": null" +
                "}";

        JSONArray jsonArray = new JSONArray(result.getResponse().getContentAsString());
        JSONObject jsonObject = jsonArray.getJSONArray(0).getJSONObject(0);
        JSONAssert.assertEquals(expected, jsonObject, false);
    }

    @Test
    public void getMSById_UnparseableUUID() throws Exception {
        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.get("/location/01234567-9ABC-DEF0-1234-" ).accept(MediaType.APPLICATION_JSON);
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().is(400))
                .andReturn();
    }

    @Test
    public void getMSById_WrongMsId() throws Exception {
        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.get("/location/01234567-9ABC-DEF0-1234-56789ABCDEF0" ).accept(MediaType.APPLICATION_JSON);
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andReturn();

        System.out.println(
                String.format("NB! Actual response GET {msId}: %s", result.getResponse().getContentAsString()));

        String expected = "{\"mobileId\": \"01234567-9abc-def0-1234-56789abcdef0\", " +
                "\"x\": 0," +
                "\"y\": 0," +
                "\"error_radius\": 0," +
                "\"error_code\": 404," +
                "\"error_description\": \"MobileStation was not detected!\"" +
                "}";

        JSONArray jsonArray = new JSONArray(result.getResponse().getContentAsString());
        JSONObject jsonObject = jsonArray.getJSONArray(0).getJSONObject(0);
        JSONAssert.assertEquals(expected, jsonObject, false);
    }

    @Test
    //@Transactional
    public void putDetectedMobileStation() throws Exception {

        String request = "{" +
                "\"bsId\": \"44cc539d-f125-49fb-b27c-ab71f37ce555\"," +
                "\"reports\": [" +
                "{\"mobile_station_id\": \"44cc539d-f125-49fb-b27c-ab71f37ce5b2\", " +
                "\"distance\": 2.0, \"timestamp\": " + timestamp + "}" +
                "]}";

        assertRequest(request);

    }

    @Test
    //@Transactional
    public void putDetectedMobileStation_BsIdNull() throws Exception {

        String request = "{" +
                "\"bsId\": null," +
                "\"reports\": [" +
                "{\"mobile_station_id\": \"44cc539d-f125-49fb-b27c-ab71f37ce5b2\", " +
                "\"distance\": 2.0, \"timestamp\": " + timestamp + "}" +
                "]}";

        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.post(Constants.POST_MAPPING_REGISTER).contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(new JSONObject(request)));
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("NotNull.base_station_id")))
                .andReturn();

        System.out.println(String.format("NB! Actual response POST: %s", result.getResponse().getContentAsString()));

    }

    @Test
    //@Transactional
    public void putDetectedMobileStation_FalseBsId() throws Exception {

        String request = "{" +
                //False bsId
                "\"bsId\": \"01234567-9ABC-DEF0-1234-56789ABCDEF0\"," +
                "\"reports\": [" +
                "{\"mobile_station_id\": \"44cc539d-f125-49fb-b27c-ab71f37ce5b2\", " +
                "\"distance\": 2.0, \"timestamp\": " + timestamp + "}" +
                "]}";

        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.post(Constants.POST_MAPPING_REGISTER).contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(new JSONObject(request)));
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("IsUUID.base_station_id")))
                .andReturn();

        System.out.println(String.format("NB! Actual response POST: %s", result.getResponse().getContentAsString()));
    }

    @Test
    //@Transactional
    public void putDetectedMobileStation_UnparseableBsId() throws Exception {

        String request = "{" +
                "\"bsId\": \"1\"," +
                "\"reports\": [" +
                "{\"mobile_station_id\": \"44cc539d-f125-49fb-b27c-ab71f37ce5b2\", " +
                "\"distance\": 2.0, \"timestamp\": " + timestamp + "}" +
                "]}";

        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.post(Constants.POST_MAPPING_REGISTER).contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(new JSONObject(request)));
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().is(400))
                .andReturn();

        System.out.println(String.format("NB! Actual response POST: %s", result.getResponse().getContentAsString()));
    }

    @Test
    //@Transactional
    public void putDetectedMobileStation_ReportsNull() throws Exception {

        String request = "{" +
                "\"bsId\": \"44cc539d-f125-49fb-b27c-ab71f37ce555\"," +
                "\"reports\": null" +
                "}";

        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.post(Constants.POST_MAPPING_REGISTER).contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(new JSONObject(request)));
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("NotNull.reports")))
                .andReturn();

        System.out.println(String.format("NB! Actual response POST: %s", result.getResponse().getContentAsString()));
    }

    @Test
    //@Transactional
    public void putDetectedMobileStation_ReportsSize0() throws Exception {

        String request = "{" +
                "\"bsId\": \"44cc539d-f125-49fb-b27c-ab71f37ce555\"," +
                "\"reports\": [" +
                "{}" +
                "]}";

        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.post(Constants.POST_MAPPING_REGISTER).contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(new JSONObject(request)));
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("NotNull.reports.mobile_station_id")))
                .andReturn();

        System.out.println(String.format("NB! Actual response POST: %s", result.getResponse().getContentAsString()));
    }

    @Test
    //@Transactional
    public void putDetectedMobileStation_MsIdNull() throws Exception {

        String request = "{" +
                "\"bsId\": \"44cc539d-f125-49fb-b27c-ab71f37ce555\"," +
                "\"reports\": [" +
                "{\"mobile_station_id\": null, " +
                "\"distance\": 2.0, \"timestamp\": " + timestamp + "}" +
                "]}";

        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.post(Constants.POST_MAPPING_REGISTER).contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(new JSONObject(request)));
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("NotNull.reports.mobile_station_id")))
                .andReturn();

        System.out.println(String.format("NB! Actual response POST: %s", result.getResponse().getContentAsString()));
    }

    @Test
    //@Transactional
    public void putDetectedMobileStation_FalseMsId() throws Exception {

        String request = "{" +
                "\"bsId\": \"44cc539d-f125-49fb-b27c-ab71f37ce555\"," +
                "\"reports\": [" +
                "{\"mobile_station_id\": \"01234567-9ABC-DEF0-1234-56789ABCDEF0\", " +
                "\"distance\": 2.0, \"timestamp\": " + timestamp + "}" +
                "]}";

        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.post(Constants.POST_MAPPING_REGISTER).contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(new JSONObject(request)));
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("IsUUID.reports.mobile_station_id")))
                .andReturn();

        System.out.println(String.format("NB! Actual response POST: %s", result.getResponse().getContentAsString()));
    }

    @Test
    //@Transactional
    public void putDetectedMobileStation_UnparseableMsId() throws Exception {

        String request = "{" +
                "\"bsId\": \"44cc539d-f125-49fb-b27c-ab71f37ce555\"," +
                "\"reports\": [" +
                "{\"mobile_station_id\": 1, " +
                "\"distance\": 2.0, \"timestamp\": " + timestamp + "}" +
                "]}";

        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.post(Constants.POST_MAPPING_REGISTER).contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(new JSONObject(request)));
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().is(400))
                .andReturn();

        System.out.println(String.format("NB! Actual response POST: %s", result.getResponse().getContentAsString()));
    }

    @Test
    //@Transactional
    public void putDetectedMobileStation_UnparseableDistance() throws Exception {

        String request = "{" +
                "\"bsId\": \"44cc539d-f125-49fb-b27c-ab71f37ce555\"," +
                "\"reports\": [" +
                "{\"mobile_station_id\": \"44cc539d-f125-49fb-b27c-ab71f37ce5b2\", " +
                "\"distance\": \"string\", \"timestamp\": " + timestamp + "}" +
                "]}";

        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.post(Constants.POST_MAPPING_REGISTER).contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(new JSONObject(request)));
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().is(400))
                .andReturn();

        System.out.println(String.format("NB! Actual response POST: %s", result.getResponse().getContentAsString()));
    }

    @Test
    //@Transactional
    public void putDetectedMobileStation_NegativeDistance() throws Exception {

        String request = "{" +
                "\"bsId\": \"44cc539d-f125-49fb-b27c-ab71f37ce555\"," +
                "\"reports\": [" +
                "{\"mobile_station_id\": \"44cc539d-f125-49fb-b27c-ab71f37ce5b2\", " +
                "\"distance\": -2.0, \"timestamp\": " + timestamp + "}" +
                "]}";

        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.post(Constants.POST_MAPPING_REGISTER).contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(new JSONObject(request)));
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Min.reports.distance")))
                .andReturn();

        System.out.println(String.format("NB! Actual response POST: %s", result.getResponse().getContentAsString()));
    }


    private void assertRequest(String request) throws Exception {

        RequestBuilder requestBuilder =
                MockMvcRequestBuilders.post(Constants.POST_MAPPING_REGISTER).contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(new JSONObject(request)));
        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andReturn();

        System.out.println(String.format("NB! Actual response POST: %s", result.getResponse().getContentAsString()));

        JSONArray jsonArray = new JSONArray(result.getResponse().getContentAsString());
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        JSONAssert.assertEquals(request, jsonObject, false);
    }

    private void createBaseStations() {
        BaseStation baseStation = new BaseStation();
        baseStation.setId(UUID.randomUUID());
        baseStation.setName("TestName_" + 1);
        baseStation.setX(2);
        baseStation.setY(1);
        baseStation.setDetectionRadiusInMeters(DETECTION_RADIUS_IN_METERS);
        baseStationService.save(baseStation);

        baseStation = new BaseStation();
        baseStation.setId(UUID.randomUUID());
        baseStation.setName("TestName_" + 2);
        baseStation.setX(4);
        baseStation.setY(8);
        baseStation.setDetectionRadiusInMeters(DETECTION_RADIUS_IN_METERS);
        baseStationService.save(baseStation);

        baseStation = new BaseStation();
        baseStation.setId(UUID.randomUUID());
        baseStation.setName("TestName_" + 3);
        baseStation.setX(9);
        baseStation.setY(5);
        baseStation.setDetectionRadiusInMeters(DETECTION_RADIUS_IN_METERS);
        baseStationService.save(baseStation);
    }

    private List<MobileStationDto> createMobileStations() {
        List<MobileStationDto> list = new ArrayList<>();

        MobileStationDto mobileStationDto = new MobileStationDto();
        mobileStationDto.setId(this.msUuid);
        mobileStationDto.setLastKnownX(5);
        mobileStationDto.setLastKnownY(4);
        list.add(0, mobileStationDto);

        return list;
    }

    private void DetectMobileStations(List<MobileStationDto> msList) {
        List<BaseStation> baseStationList = baseStationService.getAllBaseStations();
        baseStationList.forEach(baseStation -> msList.forEach(ms -> getDistance(baseStation, ms)));
    }

    private void getDistance(BaseStation baseStation, MobileStationDto mobileStationDto) {
        float msx = mobileStationDto.getLastKnownX();
        float msy = mobileStationDto.getLastKnownY();

        float distance = (float) Math.sqrt(Math.pow((baseStation.getX() - msx), 2) + Math.pow(baseStation.getY() - msy, 2));
        if (distance <= DETECTION_RADIUS_IN_METERS) {
            DetectedMs detectedMs = new DetectedMs();
            detectedMs.setBsId(baseStation.getId());
            detectedMs.setMsId(mobileStationDto.getId());
            detectedMs.setDistance(distance);
            detectedMs.setTimestamp(timestamp);

            detectedMSService.save(detectedMs);
        }
    }
}