package com.wms.demo.converter;

import com.wms.demo.dto.DetectedMsDto;
import com.wms.demo.entity.DetectedMs;

import java.util.UUID;

public class Converter {
    public static DetectedMs dtoToEntity(DetectedMsDto report, UUID bsId) {
        DetectedMs detectedMs = new DetectedMs();
        detectedMs.setBsId(bsId);
        detectedMs.setMsId(report.getMobile_station_id());
        detectedMs.setDistance(report.getDistance());
        detectedMs.setTimestamp(report.getTimestamp());

        return detectedMs;
    }
}
