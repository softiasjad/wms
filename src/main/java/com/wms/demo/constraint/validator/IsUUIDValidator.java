package com.wms.demo.constraint.validator;

import com.wms.demo.constraint.IsUUID;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsUUIDValidator implements ConstraintValidator<IsUUID, Object> {

    @Override
    public void initialize(IsUUID isUUID) {

    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        if (o == null) {
            return true;
        }

        if (o.toString()
                .matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}")) {
            return true;
        }

        return false;
    }
}
