package com.wms.demo.constraint;

import com.wms.demo.constraint.validator.IsUUIDValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD, PARAMETER, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = IsUUIDValidator.class)
@Documented
public @interface IsUUID {
    String message() default "{constraint.isUuid.default.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
