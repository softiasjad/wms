package com.wms.demo.dto;

import com.wms.demo.constraint.IsUUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public class DetectedMsMessageDto {

    @NotNull(message = "{detected.bsid.null}")
    @IsUUID(message = "{detected.bsid.notUUID}")
    private UUID base_station_id;
    @NotNull(message = "{reports.null}")
    @Valid
    private List<DetectedMsDto> reports;

    public UUID getBsId() {
        return base_station_id;
    }

    public void setBsId(UUID bsId) {
        this.base_station_id = bsId;
    }

    public List<DetectedMsDto> getReports() {
        return reports;
    }

    public void setReports(List<DetectedMsDto> reports) {
        this.reports = reports;
    }

}
