package com.wms.demo.dto;

import com.wms.demo.constraint.IsUUID;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public class DetectedMsDto {
    @NotNull(message = "{report.msId.null}")
    @IsUUID(message = "{report.msId.notUUID}")
    private UUID mobile_station_id;
    @NotNull(message = "{report.distance.null}")
    @Min(value = 0, message = "{report.distance.min}")
    private float distance;
    @NotNull(message = "{report.timestamp.null}")
    private long timestamp;

    public UUID getMobile_station_id() {
        return mobile_station_id;
    }

    public void setMobile_station_id(UUID mobile_station_id) {
        this.mobile_station_id = mobile_station_id;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
