package com.wms.demo.utils;

import com.wms.demo.dto.DetectedMsMessageDto;
import com.wms.demo.dto.MsLocationDto;
import com.wms.demo.entity.DetectedMs;
import org.springframework.http.HttpStatus;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.AssertTrue;
import java.util.*;

public class Validator {

    public static List<Object> validateMsListSize(List<DetectedMs> list, MsLocationDto msLocationDto) {
        List<MsLocationDto> error_list = new ArrayList<>();
        error_list.add(0, msLocationDto);

        if (list.size() == 0) {
            msLocationDto.setError_code(HttpStatus.NOT_FOUND.value());
            msLocationDto.setError_description(
                    "MobileStation was not detected!");
            return Collections.singletonList(error_list);
        } else if (list.size() < 3) {
            for (DetectedMs item : list) {
                if (msLocationDto.getError_radius() == 0 || msLocationDto.getError_radius() > item.getDistance()) {
                    msLocationDto.setError_radius(item.getDistance());
                }
                msLocationDto.setError_code(HttpStatus.NOT_FOUND.value());
                msLocationDto.setError_description(
                        "At least 3 BaseStations needed to calculate MobileStation coordinates!");
            }
            return Collections.singletonList(error_list);
        }
        return null;
    }
}
