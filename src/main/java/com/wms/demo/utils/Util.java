package com.wms.demo.utils;

public class Util {
    public float getDistance(float x1, float y1, float x2, float y2) {
        float distance = (float) Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
        return distance;
    }

    public static float getDistance(float[] p1, float[] p2) {
        float distance = (float) Math.sqrt(Math.pow(p1[0] - p2[0], 2) + Math.pow(p1[1] - p2[1], 2));
        return distance;
    }

    public static float toDecimals(float f, int decimals) {
        int powered = (int) Math.pow(10, decimals);
        return (float) Math.round(f * powered) / powered;
    }
}
