package com.wms.demo.utils;

public interface Constants {

    public static String HOME_MAPPING = "/wms";
    public static String GET_MAPPING_BY_UUID = "location/{uuid}";
    public static String POST_MAPPING_REGISTER = "/location/register";

    public static float DETECTION_RADIUS_IN_METERS = 5;

    public static int BASE_STATIONS_COUNT_LIMIT = 100;
    public static int MOBILE_STATIONS_COUNT_LIMIT = 100;

    public static int AREA_X_LIMIT = 100;
    public static int AREA_Y_LIMIT = 100;

    public static String CONFIG_MESSAGES_PROPERTIES = "i18/messages";
    public static String CONFIG_ERROR_MESSAGES_PROPERTIES = "i18/errormsg";
    public static String CONFIG_ENCODING_DEFAULT = "UTF-8";

    public static String LOCALE_LANGUAGE = "lang";
    public static String LOCALE_LANGUAGE_ENGLISH = "en";
    public static String LOCALE_LANGUAGE_ESTONIAN = "et";

}
