package com.wms.demo.service;

import java.util.List;
import java.util.UUID;

public interface MobileStationService {
    List<Object> getMobileStation(UUID uuid);
}
