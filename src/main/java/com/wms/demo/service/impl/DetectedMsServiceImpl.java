package com.wms.demo.service.impl;

import com.wms.demo.converter.Converter;
import com.wms.demo.dto.DetectedMsMessageDto;
import com.wms.demo.entity.DetectedMs;
import com.wms.demo.repository.DetectedMSRepository;
import com.wms.demo.service.DetectedMSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DetectedMsServiceImpl implements DetectedMSService {
    @Autowired
    DetectedMSRepository detectedMSRepository;

    @Override
    @Transactional
    public void save(DetectedMs detectedMs) {
        detectedMSRepository.save(detectedMs);
    }

    @Override
    @Transactional
    public void saveDetectedMessage(DetectedMsMessageDto detectedMsMessageDto) {
        detectedMsMessageDto.getReports()
                .forEach(report -> save(Converter.dtoToEntity(report, detectedMsMessageDto.getBsId())));
    }

    @Override
    public List<DetectedMs> getAllDetections() {
        return detectedMSRepository.findAll().stream().collect(Collectors.toList());
    }

    @Override
    public List<DetectedMs> getDetectionsByMsId(UUID msId) {
        long maxTimestamp = detectedMSRepository.getMaxTimestamp();
        return detectedMSRepository.findByMsIdAndTimestamp(msId, maxTimestamp);
    }

}
