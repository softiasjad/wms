package com.wms.demo.service.impl;

import com.wms.demo.dto.MsLocationDto;
import com.wms.demo.entity.DetectedMs;
import com.wms.demo.service.BaseStationService;
import com.wms.demo.service.DetectedMSService;
import com.wms.demo.service.MobileStationService;
import com.wms.demo.service.helper.MobileStationServiceHelper;
import com.wms.demo.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class MobileStationServiceImpl implements MobileStationService {

    @Autowired
    DetectedMSService detectedMSService;
    @Autowired
    BaseStationService baseStationService;

    public List<Object> getMobileStation(UUID uuid) {
        List<DetectedMs> list = detectedMSService.getDetectionsByMsId(uuid);
        MsLocationDto msLocationDto = new MsLocationDto();
        msLocationDto.setMobileId(uuid);

        List<Object> validated = Validator.validateMsListSize(list, msLocationDto);
        if (validated != null) {
            return validated;
        }

        msLocationDto = MobileStationServiceHelper.getMsCoordinates(msLocationDto, list, baseStationService);
        List<MsLocationDto> msLocationDtoList = new ArrayList<>();
        msLocationDtoList.add(0, msLocationDto);

        return Collections.singletonList(msLocationDtoList);
    }
}
