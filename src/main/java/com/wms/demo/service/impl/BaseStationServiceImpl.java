package com.wms.demo.service.impl;

import com.wms.demo.entity.BaseStation;
import com.wms.demo.repository.BaseStationRepository;
import com.wms.demo.service.BaseStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class BaseStationServiceImpl implements BaseStationService {

    @Autowired
    BaseStationRepository baseStationRepository;

    @Override
    @Transactional
    public void save(BaseStation baseStation) {
        baseStationRepository.save(baseStation);
    }

    @Override
    public List<BaseStation> getAllBaseStations() {
        return baseStationRepository.findAll().stream().collect(Collectors.toList());
    }

    @Override
    public BaseStation getBaseStationById(UUID uuid) {
        return baseStationRepository.getOne(uuid);
    }
}
