package com.wms.demo.service;

import com.wms.demo.dto.DetectedMsMessageDto;
import com.wms.demo.entity.DetectedMs;

import java.util.List;
import java.util.UUID;

public interface DetectedMSService {
    void save(DetectedMs detectedMs);

    List<DetectedMs> getDetectionsByMsId(UUID msId);

    void saveDetectedMessage(DetectedMsMessageDto detectedMsMessageDto);

    List<DetectedMs> getAllDetections();
}
