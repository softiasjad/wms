package com.wms.demo.service;

public interface MessageService {
    public String getMessage(String messageCode);
}
