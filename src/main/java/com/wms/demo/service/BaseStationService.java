package com.wms.demo.service;

import com.wms.demo.entity.BaseStation;

import java.util.List;
import java.util.UUID;

public interface BaseStationService {
    void save(BaseStation baseStation);

    List<BaseStation> getAllBaseStations();

    BaseStation getBaseStationById(UUID uuid);
}
