package com.wms.demo.service.helper;

import com.wms.demo.dto.MsLocationDto;
import com.wms.demo.entity.DetectedMs;
import com.wms.demo.service.BaseStationService;
import org.springframework.http.HttpStatus;

import java.util.List;

import static com.wms.demo.utils.Util.getDistance;
import static com.wms.demo.utils.Util.toDecimals;

public class MobileStationServiceHelper {
    public static MsLocationDto getMsCoordinates(MsLocationDto msLocationDto, List<DetectedMs> list, BaseStationService baseStationService) {
        //Distance between BaseStation0 and BaseStation1
        float x0 = baseStationService.getBaseStationById(list.get(0).getBsId()).getX();
        float y0 = baseStationService.getBaseStationById(list.get(0).getBsId()).getY();
        float x1 = baseStationService.getBaseStationById(list.get(1).getBsId()).getX();
        float y1 = baseStationService.getBaseStationById(list.get(1).getBsId()).getY();
        float d = getDistance(new float[]{x0, y0}, new float[]{x1, y1});

        //Distance to the intersection point between BaseStation0 and BaseStation1
        float r0 = list.get(0).getDistance();
        float r1 = list.get(1).getDistance();
        float d1 = (float) ((Math.pow(r0, 2) - Math.pow(r1, 2) + Math.pow(d, 2)) /( 2 * d));

        //Distance from intersection point to the MobileStation
        float h = (float) Math.sqrt(Math.pow(r0, 2) - Math.pow(d1, 2));

        //Intersection point coordinates
        float dx = x0 + ((d1 * (x1 - x0)) / d);
        float dy = y0 + ((d1 * (y1 - y0)) / d);

        //Possible coordinates of the MobileStation
        float msx1 = dx + (h*(y1 - y0)/d);
        float msy1 = dy - (h*(x1 - x0)/d);
        float msx2 = dx - (h*(y1 - y0)/d);
        float msy2 = dy + (h*(x1 - x0)/d);

        //BaseStation2
        float x2 = baseStationService.getBaseStationById(list.get(2).getBsId()).getX();
        float y2 = baseStationService.getBaseStationById(list.get(2).getBsId()).getY();

        //Distance between BaseStation2 and the MobileStation from detected data
        float d2 = list.get(2).getDistance();
        //Calculated distances
        float d21 = (float) Math.sqrt(Math.pow(x2 - msx1, 2) + Math.pow(y2 - msy1, 2));
        float d22 = (float) Math.sqrt(Math.pow(x2 - msx2, 2) + Math.pow(y2 - msy2, 2));

        if (toDecimals(d2, 4) == toDecimals(d21, 4)) {
            msLocationDto.setX(toDecimals(msx1, 4));
            msLocationDto.setY(toDecimals(msy1, 4));
        }else if (toDecimals(d2, 4) == toDecimals(d22, 4)) {
            msLocationDto.setX(toDecimals(msx2, 4));
            msLocationDto.setY(toDecimals(msy2, 4));
        }else {
            msLocationDto.setError_code(HttpStatus.NOT_FOUND.value());
            msLocationDto.setError_radius(d2);
            msLocationDto.setError_description(
                    String.format("Calculating MobileStation %s failed!", msLocationDto.getMobileId()));
        }

        return msLocationDto;
    }
}
