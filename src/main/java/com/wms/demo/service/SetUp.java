package com.wms.demo.service;

import com.wms.demo.dto.MobileStationDto;
import com.wms.demo.entity.BaseStation;
import com.wms.demo.entity.DetectedMs;
import com.wms.demo.repository.BaseStationRepository;
import com.wms.demo.repository.DetectedMSRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.wms.demo.utils.Constants.*;

@Service
public class SetUp {
    int x, y;
    long timestamp;
    float detectionRadiusInMeters;
    @Autowired
    BaseStationRepository baseStationRepository;
    @Autowired
    DetectedMSRepository detectedMSRepository;

    public void setInitialData() {
        this.x = (int) (Math.random() * AREA_X_LIMIT);
        this.y = (int) (Math.random() * AREA_Y_LIMIT);
        this.timestamp = Instant.now().getEpochSecond();
        this.detectionRadiusInMeters = DETECTION_RADIUS_IN_METERS;

        createBaseStations();
        List<MobileStationDto> msList = createMobileStations();
        DetectMobileStations(msList);

        //msList.forEach(ms -> System.out.println(ms.getId() + " X:" + ms.getLastKnownX() + " Y:" + ms.getLastKnownY()));
    }

    public void createBaseStations() {
        int bs = new Random().nextInt(BASE_STATIONS_COUNT_LIMIT);

        for (int i = 0; i < bs; i++) {
            BaseStation baseStation = new BaseStation();
            baseStation.setId(UUID.randomUUID());
            baseStation.setName("Name_" + i);
            baseStation.setX(new Random().nextFloat() * this.x);
            baseStation.setY(new Random().nextFloat() * this.y);
            baseStation.setDetectionRadiusInMeters(detectionRadiusInMeters);

            baseStationRepository.save(baseStation);
        }
    }

    public List<MobileStationDto> createMobileStations() {
        int ms = new Random().nextInt(MOBILE_STATIONS_COUNT_LIMIT);
        List<MobileStationDto> list = new ArrayList<>();
        for (int i = 0; i < ms; i++) {
            MobileStationDto mobileStationDto = new MobileStationDto();
            mobileStationDto.setId(UUID.randomUUID());
            mobileStationDto.setLastKnownX(new Random().nextFloat() * this.x);
            mobileStationDto.setLastKnownY(new Random().nextFloat() * this.y);

            list.add(i, mobileStationDto);
        }
        return list;
    }

    public void DetectMobileStations(List<MobileStationDto> msList) {
        List<BaseStation> baseStationList = baseStationRepository.findAll().stream().collect(Collectors.toList());
        baseStationList.forEach(baseStation -> msList.forEach(ms -> getDistance(baseStation, ms)));
    }

    private void getDistance(BaseStation baseStation, MobileStationDto mobileStationDto) {
        float msx = mobileStationDto.getLastKnownX();
        float msy = mobileStationDto.getLastKnownY();

        float distance = (float) Math.sqrt(Math.pow((baseStation.getX() - msx), 2) + Math.pow(baseStation.getY() - msy, 2));
        if (distance <= detectionRadiusInMeters) {
            DetectedMs detectedMs = new DetectedMs();
            detectedMs.setBsId(baseStation.getId());
            detectedMs.setMsId(mobileStationDto.getId());
            detectedMs.setDistance(distance);
            detectedMs.setTimestamp(timestamp);

            detectedMSRepository.save(detectedMs);
        }
    }
}
