package com.wms.demo.repository;

import com.wms.demo.entity.DetectedMs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface DetectedMSRepository extends JpaRepository<DetectedMs, Integer> {
    @Query("SELECT MAX(timestamp) FROM DetectedMs")
    long getMaxTimestamp();

    List<DetectedMs> findByMsIdAndTimestamp(UUID msId, long timestamp);
}
