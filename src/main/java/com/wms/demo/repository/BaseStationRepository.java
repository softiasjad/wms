package com.wms.demo.repository;

import com.wms.demo.entity.BaseStation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BaseStationRepository extends JpaRepository<BaseStation, UUID> {
}
