package com.wms.demo;

import com.wms.demo.service.SetUp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class BootDemoApplication {

	/*
	http://localhost:8080/wms
	 */

    @Autowired
    SetUp setUp;

    public static void main(String[] args) {
        SpringApplication.run(BootDemoApplication.class, args);
    }

    @PostConstruct
    public void setupDbWithData() {
        setUp.setInitialData();
    }
}
