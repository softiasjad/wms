package com.wms.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class DetectedMs {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private UUID bsId;
    private UUID msId;
    private float distance;
    private long timestamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UUID getBsId() {
        return bsId;
    }

    public void setBsId(UUID bsId) {
        this.bsId = bsId;
    }

    public UUID getMsId() {
        return msId;
    }

    public void setMsId(UUID msId) {
        this.msId = msId;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

}
