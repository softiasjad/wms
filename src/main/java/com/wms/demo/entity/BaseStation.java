package com.wms.demo.entity;

import com.wms.demo.constraint.IsUUID;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class BaseStation implements Serializable {

    @Id
    @Column(unique = true, updatable = false)
    @IsUUID(message = "{baseStation.id.notUUID}")
    private UUID id;
    @NotBlank(message = "{baseStation.name.blank}")
    private String name;
    @NotNull(message = "{baseStation.x.null}")
    private float x;
    @NotNull(message = "{baseStation.y.null}")
    private float y;
    @NotNull(message = "{baseStation.radius.null}")
    private float detectionRadiusInMeters;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getDetectionRadiusInMeters() {
        return detectionRadiusInMeters;
    }

    public void setDetectionRadiusInMeters(float detectionRadiusInMeters) {
        this.detectionRadiusInMeters = detectionRadiusInMeters;
    }

}
