package com.wms.demo.controller;

import com.wms.demo.entity.DetectedMs;
import com.wms.demo.repository.DetectedMSRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.wms.demo.utils.Constants.HOME_MAPPING;

@RestController
public class HomeController {

    /**
     * For GUI e.g: return "index"; Location: resources.static.views
     * See also application.properties
     * For any case I provided folders for frontend css and js as well
     */

    @Autowired
    DetectedMSRepository msRepository;


    @RequestMapping(HOME_MAPPING)
    public List<DetectedMs> getList() {
        List<DetectedMs> list = msRepository.findAll().stream().collect(Collectors.toList());
        list.sort((s1, s2) -> s1.getMsId().compareTo(s2.getMsId()));
        return list;
    }
}
