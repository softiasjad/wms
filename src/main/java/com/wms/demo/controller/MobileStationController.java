package com.wms.demo.controller;

import com.wms.demo.dto.DetectedMsMessageDto;
import com.wms.demo.service.DetectedMSService;
import com.wms.demo.service.MobileStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static com.wms.demo.utils.Constants.GET_MAPPING_BY_UUID;
import static com.wms.demo.utils.Constants.POST_MAPPING_REGISTER;

@RestController
public class MobileStationController {

    @Autowired
    DetectedMSService detectedMSService;

    @Autowired
    MobileStationService mobileStationService;


    @GetMapping(GET_MAPPING_BY_UUID)
    public List<Object> getMobileStationById(@PathVariable UUID uuid) {
        return mobileStationService.getMobileStation(uuid);
    }

    @PostMapping(POST_MAPPING_REGISTER)
    public List<Object> setDetectedMobileStation(
            @Validated @RequestBody DetectedMsMessageDto detectedMsMessageDto, BindingResult result) {

        if (result.hasErrors()) {
            return Collections.singletonList(result.getAllErrors());
        }

        detectedMSService.saveDetectedMessage(detectedMsMessageDto);

        return Collections.singletonList(detectedMsMessageDto);
    }
}
